package com.dexciuq.calculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.dexciuq.calculator.fragments.ConstraintFragment
import com.dexciuq.calculator.fragments.FrameFragment
import com.dexciuq.calculator.fragments.LinearFragment

class CalculatorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)

        val fragment = when(intent.extras?.getString("fragment_name")) {
            "FRAME" -> FrameFragment()
            "LINEAR" -> LinearFragment()
            "CONSTRAINT" -> ConstraintFragment()
            else -> throw IllegalArgumentException("Unknown fragment name")
        }

        supportFragmentManager.commit {
            add(R.id.fragment_container, fragment)
        }
    }
}