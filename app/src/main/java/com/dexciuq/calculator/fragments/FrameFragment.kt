package com.dexciuq.calculator.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.dexciuq.calculator.R

class FrameFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_frame, container, false)
        val editText = view.findViewById<EditText>(R.id.edit_text)

        listOf<Button>(
            view.findViewById(R.id.one),
            view.findViewById(R.id.two),
            view.findViewById(R.id.three),
            view.findViewById(R.id.four),
            view.findViewById(R.id.five),
            view.findViewById(R.id.six),
            view.findViewById(R.id.seven),
            view.findViewById(R.id.eight),
            view.findViewById(R.id.nine),
            view.findViewById(R.id.plus),
            view.findViewById(R.id.minus),
            view.findViewById(R.id.multiply)
        ).forEach { button ->
            button.setOnClickListener {
                editText.append((it as Button).text)
            }
        }

        return view
    }
}