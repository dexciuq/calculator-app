package com.dexciuq.calculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.dexciuq.calculator.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        listOf(binding.frameButton, binding.linearButton, binding.constraintButton).forEach { btn ->
            btn.setOnClickListener {
                val intent = Intent(this, CalculatorActivity::class.java)
                intent.putExtra("fragment_name", btn.extractFragmentName())
                startActivity(intent)
            }
        }
    }

    private fun Button.extractFragmentName() : String = when(this.id) {
        R.id.frame_button -> "FRAME"
        R.id.linear_button -> "LINEAR"
        R.id.constraint_button -> "CONSTRAINT"
        else -> throw IllegalArgumentException("Unknown button")
    }
}